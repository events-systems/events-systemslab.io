`sctlib` is a quick starter for anyone wanting to have their libli client instance hosted on a gitlab page (using gitlab ci/cd).

> is is also a test project using a default libli element on a root domain and its own matrix hosting (though not required, on `matrix.domain.tld`).

# libli?

Libli is a (light) [Matrix](https://matrix.org) client, for accessing the content of public matrix rooms.

- https://gitlab.com/sctlib/libli
- https://www.npmjs.com/package/@sctlib/libli

It is a web component, HTML DOM element, usable in any web page as `<lib-li></lib-li>`.

# Get started

## libli, frontend client instance
1. fork this project to your gitlab's instance account
1. edit the values of `.env.json` (for local development) and
   `.env.production.json` for your live deployment
1. check the CI/CD to see if your gitlab page got deployed
1. read both project's readme for more information

> libli configuration and documentation is available in libli's git readme

## libli, bound to a matrix server instance

You can host libli at the root of a domain you own, which has also a matrix server on a subdomain.

1. [get a domain name your like](https://njal.la/)
1. [Host your own matrix server](https://etke.cc/) on one of your domains's subdomain
1. edit the files `.well-known/matrix/{client,server}` with the correct values
1. edit `.env.production.json` wiwht the correct value for `"platform-server-url": "https://sctlib.org"` and `"index-profile": "#welcome:sctlib.org"`

Now all your server's matrix public room, should appear on your domain where libli is hosted, as normally served HTML pages (but with a javascript web-component, inspect your webpages DOM).

# Need help, assistance, comments, feature request, hello?

> open an issue on this repository; and/or come chat in one of the
> matrix.org rooms; https://matrix.to/#/#libli:matrix.org
